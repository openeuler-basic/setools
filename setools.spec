%global debug_package %{nil}
Name:		setools
Version:	4.3.0
Release:	4
Summary:	Policy Analysis Tools for SELinux
License:	GPLv2
URL:		https://github.com/SELinuxProject/setools/wiki
Source0:	https://github.com/SELinuxProject/setools/archive/%{version}.tar.gz
Source1:        setools.pam
Source2:        apol.desktop

Patch0:         0001-Do-not-use-Werror-during-build.patch
Patch1:         0002-Do-not-export-use-setools.InfoFlowAnalysis-and-setoo.patch
Patch2:         0003-Require-networkx-on-package-level.patch

BuildRequires:	flex bison glibc-devel gcc swig git python3-setuptools 
BuildRequires:  qt5-qtbase-devel python3-devel
BuildRequires:  libsepol-devel >= 3.1 libsepol-static >= 3.1
BuildRequires:	python3-Cython
BuildRequires:	python3-setuptools
Obsoletes:      setools < 4.0.0, setools-devel < 4.0.0

%description
SETools consists of a number of SELinux policy analysis tools, both graphical 
and command line.

%package        console
Summary:        Console packages for setools
License:        GPLv2
Requires:       python3-setools = %{version}-%{release} libselinux >= 3.1

%description    console
Console packages for setools.

%package        console-analyses
Summary:        Analyses files for setools-console
License:        GPLv2
Requires:       python3-setools = %{version}-%{release} 
Requires:       libselinux >= 3.1

%description    console-analyses
Analyses files for setools-console.

%package     -n python3-setools
Summary:        Python3 package for setools
Requires:       python3-setuptools
Provides:       setools-python3 = %{version}-%{release}
Obsoletes:      setools-libs < 4.0.0
Obsoletes:      setools-python3 < %{version}-%{release}
Obsoletes:      setools < %{version}-%{release}
Recommends:     libselinux-python3
%{?python_provide:%python_provide python3-setools}

%description -n python3-setools
Python3 package for setools.

%package        gui
Summary:        Gui packages for setools
Requires:       python3-setools = %{version}-%{release}

%description    gui
Gui packages for setools.

%package_help

%prep
%autosetup -n setools-%{version} -p1

%build
%{__python3} setup.py build

%install
rm -rf %{buildroot}%{_bindir}
%{__python3} setup.py install --root %{buildroot}
%py3_install

%check
%if %{?_with_check:1}%{!?_with_check:0}
%{__python3} setup.py test
%endif

%files

%files          console
%defattr(-,root,root)
%{_bindir}/sediff
%{_bindir}/seinfo
%{_bindir}/sesearch

%files          console-analyses
%defattr(-,root,root)
%{_bindir}/sedta
%{_bindir}/seinfoflow

%files -n       python3-setools
%defattr(-,root,root)
%license COPYING COPYING.GPL COPYING.LGPL
%{python3_sitearch}/setools
%{python3_sitearch}/setools-%{version}-*

%files          gui
%defattr(-,root,root)
%{_bindir}/apol
%{python3_sitearch}/setoolsgui
%{_mandir}/man1/apol*
%{_mandir}/ru/man1/apol*

%files          help
%defattr(-,root,root)
%{_mandir}/man1/* 
%{_mandir}/ru/man1/* 

%changelog
* Tue Dec 1 2020 Liquor <lirui130@huawei.com> - 4.3.0-4
- add the necessary version dependencies

* Tue Aug 11 2020 linwei <linwei54@huawei.com> - 4.3.0-3
- remove requires python3-network

* Thu Aug 6 2020 Liquor <lirui130@huawei.com> - 4.3.0-2
- remove requires python3-networkx

* Thu Jul 30 2020 linwei <linwei54@huawei.com> - 4.3.0-1
- update setools to 4.3.0

* Fri Mar 13 2020 zhangrui <zhangrui182@huawei.com> - 4.1.1-17
- obsolete setools

* Fri Mar 6 2020 songnannan <songnannan2@huaiwei.com> - 4.1.1-16
- remove the unnecessary requires

* Wed Oct 30 2019 yanan <yanan@huawei.com> - 4.1.1-15
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add global marco of debug_package

* Tue Sep 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.1.1-14
- Package init
